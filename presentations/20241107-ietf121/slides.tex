\input{shared/ietf-slides.tex}

% about the presentation
\title{Building onion routing from CoRE building blocks}
\subtitle{\texttt{draft-amsuess-t2trg-onion-coap-02}}
\author{\textit{Christian~Amsüss}, Marco~Tiloca, Rikard~Höglund}
\date{IETF121 Dublin, CoRE, 2024-11-07}

\usepackage{listings}
\lstset{basicstyle=\ttfamily}
% trick from https://tex.stackexchange.com/questions/15237/highlight-text-in-code-listing-while-also-keeping-syntax-highlighting/19250#19250
\lstdefinestyle{emphasised}{
    moredelim=**[is][\color{cyan}]{`}{`},
    moredelim=**[is][\color{red}]{@}{@},
}

\begin{document}

\frame{\titlepage}

\begin{frame}{What this is about}
  \framesubtitle{A request's path through an onion style network}
  \includegraphics{../20231103-ietf118/network.pdf}

  Proxies selected randomly by server (P5, P7, P2, P4) and client (P3, P6, P1)
\end{frame}

% cabo's shopping list:
%
%     using -oscore-capable-proxies as building block
%     how to encode addresses of hidden services and "introduction-point" proxies in a URI.
%     adding new CoAP options for instructing reverse-proxies about a reverse path to enforce.

\begin{frame}{CoRE building blocks this uses}\large
  \framesubtitle{$\alpha$ early, $\beta$ mature, $\qedsymbol$ published, $\ast$ just for this, $?$ unsure classification}
  \begin{itemize}
    \item[$\qedsymbol$] EDHOC and OSCORE
    \item[$\beta$] OSCORE in OSCORE (\ietfdraft{ietf-core-oscore-capable-proxies}),\\
      including its source routing for requests
    \item[$\beta$] Advertising proxies (\ietfdraft{ietf-core-transport-indication})
    \item[$\alpha$] Naming by hashes (\ietfdraft{ietf-core-transport-indication})
      \\ ($\ast$ deriving names from EDHOC identities)
    \item[$\beta^?$] Resource Directory (RD) doubling as proxy (\ietfdraft{amsuess-core-resource-directory-extensions})
      \\ with some extras (optimizations?) in this document
    \item[$\ast^?$] Globally federated address database (and how to tell RD to register there) % Might just as well have that for global names if you can, like, show your DANE fits
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{OSCORE in OSCORE $\beta$}\large
  \framesubtitle{Source routing from the client}

\begin{lstlisting}[style=emphasised]
POST to coap://proxy1
OSCORE: Encrypted with context client-proxy1      `\`
{ POST                                            `| repeated`
  Proxy-Scheme: coap                              `| layers`
  Uri-Host: proxy2                                `/`
  OSCORE: Encrypted with context client-proxy2    `\`
  { @POST@                                          `|`
    Proxy-Scheme: coap                            `|`
    @Uri-Host: server@                              `/`
    @OSCORE: Encrypted with context client-server
    { GET
      Uri-Path: /.well-known/core
}@ } }
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{OSCORE in OSCORE $\beta$: Setup through EDHOC $?$}\large
  \framesubtitle{Step by step establishment}

\begin{lstlisting}[style=emphasised]
POST to coap://proxy1
OSCORE: Encrypted with context client-proxy1      `\`
{ POST                                            `| n`
  Proxy-Scheme: coap                              `| layers`
  Uri-Host: proxy2                                `/`
  OSCORE: Encrypted with context client-proxy2
  { POST
    Uri-Path: /.well-known/edhoc
    Payload:
    EDHOC message 1
    `(establishing (n+1)th level of nesting)`

   } }
\end{lstlisting}
\end{frame}

\begin{frame}{Naming things with hashes $\alpha$}\large
  \framesubtitle{\ietfdraft{ietf-core-transport-indication}}

  \texttt{coap://edhoc-cred.uehks...3xjdqa.service.arpa}

  \bigskip

  ``You will recognize the server by this KCCS\footnote{EDHOC/COSE lingo for raw public key}.''

  \vspace{2cm}

  Other use cases: Expressing SVCB in literals; providing EDHOC authentication criteria in DNS-over-CoAP discovery.
\end{frame}

\begin{frame}{Advertising proxies through hyperlinks $\beta$}\large
  \framesubtitle{\ietfdraft{ietf-core-transport-indication}}

  \texttt{<coap://[2001:db8::1]>;rel=has-proxy;\textbackslash}\\
  \texttt{anchor="coap://edhoc-cred.uehks\ldots3xjdqa.service.arpa"}

  \bigskip

  (a link from \texttt{coap://edh…arpa} with the relation \texttt{has-proxy} to \texttt{coap://[…]}).

  \bigskip

  ``In order to reach that the \texttt{uehks\ldots} URI, you can go through \texttt{2001:db8::1}.''

  \vspace{2cm}

  Other use cases: providing pre-resolved names; discovering CoAP transports.

\end{frame}

\begin{frame}[fragile]{Resource Directory for storing statements $\qedsymbol$}\large
  \framesubtitle{\rfc{9176}}

  \begin{itemize}
    \item POST some links to the resource directory
    \item Others can find those links.
  \end{itemize}

  \bigskip

  Example links:

  \bigskip

  \begin{lstlisting}[style=emphasised]
POST to coap://rd-host
Uri-Path: register
Payload:
</hello>
  \end{lstlisting}

  RD infers the endpoint's address as base address
\end{frame}

\begin{frame}[fragile]{Adding in RD extensions $\beta^?$}\large
  \framesubtitle{\ietfdraft{amsuess-core-resource-directory-extensions}, \ietfdraft{ietf-core-groupcomm-proxy}}

  ``Here are my links, and please proxy for me''

  \bigskip

  \ldots but what if I am running virtual hosting?
  Role reversal address\footnote{Implied in existing RFCs, but lacking definition.} is only an IP literal.

  \bigskip

  Suspecting to overlap with \texttt{Reply-From} option:
  ``When interacting with me as a server, call me this in \texttt{Uri-Host}''

  \vspace{1cm}

  Is this strictly necessary or merely an optimization?

  \bigskip

  Is the role reversal address of an EDHOC established connection
  just the IP, or maybe constructed into \texttt{coap://edhoc-cred.uehks...3xjdqa.service.arpa}?
\end{frame}

\begin{frame}[fragile]{Reverse proxying without hop-by-hop RD registration $\ast^?$}\large
  Proposed option: ``Be prepared to send role reversal my way.''

  \bigskip

  Sets up reverse proxy ad hoc; relevant only when performing actions that cause role reversal.

  \bigskip

  Hop-by-hop, but set on each hop.

  \vspace{1cm}

  Similar semantics in ICNRG (``reflexive forwarding'').

  \vspace{1cm}

  Proxy may need to use \texttt{Reply-From} for multiplexing.
\end{frame}

\begin{frame}{Full sequence: Registering a hidden service}\large
  \framesubtitle{S-P1-P2-P3; P3 is member of a federated RD}
  \begin{enumerate}
    \item Establish EDHOC with P1
    \item Using that as a forward proxy, establish EDHOC with P2
    \item Using that as a forward proxy, establish EDHOC with P3
    \item Through that, register at P3 RD.
      \begin{enumerate}
        \item Allow-Role-Reverse inner option to P1
        \item P1 sets Allow-Role-Reverse option to P2, may need \texttt{Reply-From} for multiplexing; P2-P3 likewise
        \item P3 sees inner registration request.
        \item P3 sees ``proxy for me'', and adds link to itself as proxy for the registered hidden service name.
      \end{enumerate}
    \item Clients find cryptographic name in federated RD, and P3 as proxy address.
  \end{enumerate}
\end{frame}

\begin{frame}\Huge
  Questions?

  \bigskip

  Suggestions?
\end{frame}

\end{document}
